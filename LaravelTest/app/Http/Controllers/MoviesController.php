<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MoviesController extends Controller
{
	public function showForm()
	{
		return view('forma-film');
	}

	public function addMovie(Request $request)
	{
		$id = $request->get('id');
		$title = $request->get('title');
		$year = $request->get('year');
		$director = $request->get('director');
		$actors = $request->get('actors');

		$rules = [
				'id' => 'required|numeric',
				'title' => 'required|alpha',
				'year' => 'required|numeric',
				'director' => 'required|alpha',
				'actors' => 'required|alpha'
				];


		$validator = \Validator::make($request->all(), $rules);

		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator);
		}

		return view('film-podatoci', [
			'id' => $id,
			'title' => $title,
			'year' => $year,
			'director' => $director,
			'actors' => $actors
		]);

	}
}
